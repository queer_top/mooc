package org.nathan.Anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 文件名称：@title: ColName
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:数据库注解
 * 创建日期：@date: 2024/3/28 1:57
 */

@Retention(RetentionPolicy.RUNTIME)//声明当前注解在运行时阶段起作用
@Target(ElementType.FIELD)//声明作用范围
public @interface ColName {
    String value();//属性
}
