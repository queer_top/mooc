package org.nathan.dao;

import org.nathan.Anno.ColName;
import org.nathan.utils.JDBCUtils;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 文件名称：@title: BaseDap
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:简单的增删改查
 * 创建日期：@date: 2024/3/28 0:26
 */
public abstract class BaseDao {
    // 增删改
    public int update(String sql, Object... params) {
        Connection conn = null;
        PreparedStatement statement = null;
        try {
            conn = JDBCUtils.createConn();
            statement = conn.prepareStatement(sql);
            System.out.println(Arrays.toString(params) + "----------base27");
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            return statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtils.closeConnect(conn, statement, null);
        }
    }

    // 查
    public <T> List<T> select(String sql, Class cl, Object... params) {
        List list = new ArrayList();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            conn = JDBCUtils.createConn();
            statement = conn.prepareStatement(sql);
            System.out.println(Arrays.toString(params) + "----------base51");
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            rs = statement.executeQuery();

            while (rs.next()) {
                T o = (T) cl.newInstance();
                //根据属性名获取数据库中对应字段的值
                Field fields[] = cl.getDeclaredFields();
                for (Field field : fields) {
                    ColName colName = field.getAnnotation(ColName.class);
                    Object value = rs.getObject(colName.value());
                    field.setAccessible(true);
                    field.set(o, value);
                }
                if (o != null)
                    list.add(o);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtils.closeConnect(conn, statement, rs);
        }
        return list;
    }
}