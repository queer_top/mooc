package org.nathan.dao;

import java.util.HashMap;
import java.util.Map;

/**
 * 文件名称：@title: DaoFactory
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:Dao层工厂
 * 创建日期：@date: 2024/3/28 0:29
 */
public class DaoFactory {
    private static Map<Class, Object> daoMap = new HashMap<>();

    static {
        daoMap.put(UserDao.class, new UserDaoImpl());
    }

    public static <T> T getDao(Class cl) {
        return (T) daoMap.get(cl);
    }
}
