package org.nathan.dao;

import org.nathan.pojo.User;

/**
 * 文件名称：@title: UserDao
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:用户工具接口
 * 创建日期：@date: 2024/3/28 0:25
 */
public interface UserDao {
    //登入检测
    User queryUserByAccountAndPassword(String acc, String pwd);

    // 注册用户名是否重复检测
    boolean queryUserByAccount(String acc);

    // 注册
    int insertUser(User user);

    // 查找账号所有信息
    User queryUserInfoByUserId(Integer userId);

    // 修改密码时判断旧密码是否与原本相同
    boolean queryPasswordByOldPassword(Integer userId, String oldPassWord);

    // 插入新的密码
    int updateNewPassword(Integer userId, String newPassword);
}
