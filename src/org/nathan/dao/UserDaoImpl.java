package org.nathan.dao;

import org.nathan.pojo.User;

import java.util.List;

/**
 * 文件名称：@title: UserDao
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:用户层数据库连接
 * 创建日期：@date: 2024/3/28 0:43
 */
public class UserDaoImpl extends BaseDao implements UserDao {

    @Override
    public User queryUserByAccountAndPassword(String acc, String pwd) {
        String sql = "select * from t_user where u_account = ? and u_password = ? and u_role = 'user'";
        List list = super.select(sql, User.class, acc, pwd);
        if (list.size() != 0) {
            return (User) list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public boolean queryUserByAccount(String acc) {
        String sql = "select * from t_user where u_account = ? ;";
        List list = super.select(sql, User.class, acc);
        if (list.size() == 0) {
            return true;
        }
        return false;
    }

    @Override
    public int insertUser(User user) {
        String sql = "insert into t_user(u_account,u_password,u_nickname,u_phone,u_email,u_sex) values(?,?,?,?,?,?)";
        return super.update(sql, user.getAccount(), user.getPassword(), user.getNickname(), user.getPhone(),
                user.getEmail(), user.getSex());
    }

    @Override
    public User queryUserInfoByUserId(Integer userId) {
        String sql = "select * from t_user where u_id = ?";
        List list = super.select(sql, User.class, userId);
        if (list.size() != 0) {
            return (User) list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public boolean queryPasswordByOldPassword(Integer userId, String oldPassWord) {
        String sql = "select * from t_user where u_id = ? and u_password = ? ";
        List list = super.select(sql, User.class, userId, oldPassWord);
        if (list.size() == 0) {
            return false;

        }

        return true;
    }

    @Override
    public int updateNewPassword(Integer userId, String newPassword) {
        String sql = "update t_user set u_password = ? where u_id = ?";
        return super.update(sql, newPassword, userId);
    }
}
