package org.nathan.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * 文件名称：@title: Result
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:结果数据传输对象
 * 创建日期：@date: 2024/3/28 3:33
 */
public class Result {
    private int statusCode = 200;
    // 状态码  200正确   404页面不存在
    // 201登入时验证码输入错误，301登入时用户名密码有误，401注册时账号已存在，501注册时注册失败错误
    // 701展示个人信息时用户id错误，801修改密码时旧密码不同，901修改密码时密码修改失败
    private String errMsg;// 错误消息
    private Map<String, Object> data = new HashMap<>();// 数据

    // get和set方法
    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }


}
