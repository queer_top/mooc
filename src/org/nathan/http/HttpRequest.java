package org.nathan.http;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 文件名称：@title: HttpRequest
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:http协议解析
 * 创建日期：@date: 2024/3/28 9:31
 */
public class HttpRequest {
    private String msg;
    private String requestLine; // 请求行
    private String requestMethod; // 请求方式
    private String requestUrl; // 请求路径
    private Map<String, String> requestParams = new HashMap<>(); // 请求参数
    private Map<String, String> requestHeaders = new HashMap<>(); // 请求头
    private String requestBody; // 请求体

    public HttpRequest(String recvMsg) {
        this.msg = msg;
        parseHttpProtecol(recvMsg);
    }

    public void parseHttpProtecol(String msg) {
        System.out.println("解析" + msg);
        // 解析完成后将对应的解析结果填充到对象的属性中
        String[] rows = msg.split("\r\n");
        requestLine = rows[0];
        String[] cols = requestLine.split(" ");
        requestMethod = cols[0];
        requestUrl = cols[1];
        //判断是get还是post请求
        if ("post".equalsIgnoreCase(requestMethod)) {
            try {
                requestBody = URLDecoder.decode(rows[rows.length - 1], "utf-8");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
            String info[] = requestBody.split("&");
            for (String s : info) {
                String temp[] = s.split("=");
                requestParams.put(temp[0], temp[1]);
            }
        } else if ("GET".equalsIgnoreCase(requestMethod)) {
            if (requestUrl.contains("?")) {
                String temp[] = requestUrl.split("\\?");
                requestUrl = temp[0];
                String s = null;
                try {
                    s = URLDecoder.decode(temp[1], "utf-8");
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException(e);
                }
                String str = s.split(" ")[0];
                String info[] = str.split("&");
                for (String s1 : info) {
                    String temp1[] = s.split("=");
                    requestParams.put(temp[0], temp[1]);
                }
            }
        }

    }

    //展示请求参数
    public void showRequestParams() {
        //键的遍历
        Set<String> keySet = getRequestParams().keySet();
        for (String k : keySet) {
            System.out.println("请求参数：" + k + "-----" + getRequestParams().get(k));
        }
    }

    //展示请求头
    public void showRequestHeaders() {
        //键值对遍历
        Set<Map.Entry<String, String>> entries = getRequestHeaders().entrySet();
        for (Map.Entry<String, String> entry : entries) {
            System.out.println("请求头：" + entry.getKey() + "----------" + entry.getValue());
        }
    }

    public String getMsg() {
        return msg;
    }

    public String getRequestLine() {
        return requestLine;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public Map<String, String> getRequestParams() {
        return requestParams;
    }

    public Map<String, String> getRequestHeaders() {
        return requestHeaders;
    }

    public String getRequestBody() {
        return requestBody;
    }
}
