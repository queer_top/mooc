package org.nathan.http;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

/**
 * 文件名称：@title: HttpResponse
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:http协议响应
 * 创建日期：@date: 2024/3/28 9:31
 */

public class HttpResponse {
    private int statusCode = 200;//响应状态码
    private String statusMsg = "OK"; //响应状态消息
    private String contentType = "application/json;charset=utf-8";
    private OutputStream os;

    public HttpResponse(OutputStream os) {
        this.os = os;
    }

    //字符串
    public void write(String s) {
        try {
            write(s.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    // 字节数组
    public void write(byte data[]) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("HTTP/1.1 " + statusCode + " " + statusMsg + "\r\n");
            sb.append("Content-Type: " + contentType + "\r\n");
            sb.append("Content-Length: " + data.length + "\r\n");
            sb.append("\r\n");
            os.write(sb.toString().getBytes("utf-8"));
            os.write(data);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //get和set方法
    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
