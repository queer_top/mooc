package org.nathan.pojo;

import org.nathan.Anno.ColName;

import java.time.LocalDateTime;

/**
 * 文件名称：@title: chapter
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:章节
 * 创建日期：@date: 2024/3/28 15:12
 */
public class Chapter {

    @ColName("c_id")
    private Integer chapId; // 章节id

    @ColName("c_leve")
    private String chapLeve; // 章节层级

    @ColName("c_name")
    private String chapName; // 章节名称

    @ColName("c_video")
    private String chapVideo; // 章节视频

    @ColName("c_uploadtime")
    private LocalDateTime uploadTime; // 视频上传时间

    @ColName("course_id")
    private Integer courseId; // 课程id

    // 无参构造
    public Chapter() {
    }

    // 有参构造
    public Chapter(Integer chapId, String chapLeve, String chapName, String chapVideo, LocalDateTime uploadTime,
                   Integer courseId) {
        this.chapId = chapId;
        this.chapLeve = chapLeve;
        this.chapName = chapName;
        this.chapVideo = chapVideo;
        this.uploadTime = uploadTime;
        this.courseId = courseId;
    }

    // get和set
    public Integer getChapId() {
        return chapId;
    }

    public void setChapId(Integer chapId) {
        this.chapId = chapId;
    }

    public String getChapLeve() {
        return chapLeve;
    }

    public void setChapLeve(String chapLeve) {
        this.chapLeve = chapLeve;
    }

    public String getChapName() {
        return chapName;
    }

    public void setChapName(String chapName) {
        this.chapName = chapName;
    }

    public String getChapVideo() {
        return chapVideo;
    }

    public void setChapVideo(String chapVideo) {
        this.chapVideo = chapVideo;
    }

    public LocalDateTime getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(LocalDateTime uploadTime) {
        this.uploadTime = uploadTime;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    // 测试
    @Override
    public String toString() {
        return "Chapter{" +
                "chapId=" + chapId +
                ", chapLeve='" + chapLeve + '\'' +
                ", chapName='" + chapName + '\'' +
                ", chapVideo='" + chapVideo + '\'' +
                ", uploadTime=" + uploadTime +
                ", courseId=" + courseId +
                '}';
    }
}
