package org.nathan.pojo;

import org.nathan.Anno.ColName;

/**
 * 文件名称：@title: Classify
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:分类
 * 创建日期：@date: 2024/3/28 15:12
 */
public class Classify {

    @ColName("c_id")
    private Integer classifyID; // 分类id

    @ColName("c_name")
    private String classifyName; // 分类名称

    // 无参构造
    public Classify() {
    }

    // 有参构造
    public Classify(Integer classifyID, String classifyName) {
        this.classifyID = classifyID;
        this.classifyName = classifyName;
    }

    // get和set
    public Integer getClassifyID() {
        return classifyID;
    }

    public void setClassifyID(Integer classifyID) {
        this.classifyID = classifyID;
    }

    public String getClassifyName() {
        return classifyName;
    }

    public void setClassifyName(String classifyName) {
        this.classifyName = classifyName;
    }

    // 测试

    @Override
    public String toString() {
        return "Classify{" +
                "classifyID=" + classifyID +
                ", classifyName='" + classifyName + '\'' +
                '}';
    }
}
