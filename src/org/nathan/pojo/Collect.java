package org.nathan.pojo;

import org.nathan.Anno.ColName;

/**
 * 文件名称：@title: Collect
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:收藏
 * 创建日期：@date: 2024/3/28 15:13
 */
public class Collect {

    @ColName("c_id")
    private Integer coltId; // 收藏id

    @ColName("course_id")
    private Integer courseId; // 课程id

    @ColName("user_id")
    private Integer userId; // 用户id

    // 无参构造
    public Collect() {
    }

    // 有参构造
    public Collect(Integer coltId, Integer courseId, Integer userId) {
        this.coltId = coltId;
        this.courseId = courseId;
        this.userId = userId;
    }

    // get和set
    public Integer getColtId() {
        return coltId;
    }

    public void setColtId(Integer coltId) {
        this.coltId = coltId;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    //测试
    @Override
    public String toString() {
        return "Collect{" +
                "coltId=" + coltId +
                ", courseId=" + courseId +
                ", userId=" + userId +
                '}';
    }
}
