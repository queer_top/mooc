package org.nathan.pojo;

import org.nathan.Anno.ColName;

import java.time.LocalDateTime;

/**
 * 文件名称：@title: Comment
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:评论
 * 创建日期：@date: 2024/3/28 15:13
 */
public class Comment {

    @ColName("c_id")
    private Integer commentId; // 评论id

    @ColName("c_content")
    private String content; // 评论内容

    @ColName("c_time")
    private LocalDateTime contTime; // 评论时间

    @ColName("course_id")
    private Integer courseId; // 课程id

    @ColName("user_id")
    private Integer userId; // 用户id

    // 无参构造
    public Comment() {
    }

    // 有参构造
    public Comment(Integer commentId, String content, LocalDateTime contTime, Integer courseId, Integer userId) {
        this.commentId = commentId;
        this.content = content;
        this.contTime = contTime;
        this.courseId = courseId;
        this.userId = userId;
    }

    // get和set
    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getContTime() {
        return contTime;
    }

    public void setContTime(LocalDateTime contTime) {
        this.contTime = contTime;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    // 测试

    @Override
    public String toString() {
        return "Comment{" +
                "commentId=" + commentId +
                ", content='" + content + '\'' +
                ", contTime=" + contTime +
                ", courseId=" + courseId +
                ", userId=" + userId +
                '}';
    }
}
