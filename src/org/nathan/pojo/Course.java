package org.nathan.pojo;

import org.nathan.Anno.ColName;

import java.time.LocalDateTime;

/**
 * 文件名称：@title: Course
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:课程
 * 创建日期：@date: 2024/3/28 15:13
 */
public class Course {

    @ColName("c_id")
    private Integer courseId; // 课程id

    @ColName("c_name")
    private String courseName; // 课程名称

    @ColName("c_type")
    private String courseType; // 课程类型 0-免费 1-付费

    @ColName("c_intro")
    private String courseIntro; // 课程简介

    @ColName("c_img")
    private String courseImg; // 课程封面

    @ColName("c_price")
    private Double coursePrice; // 课程价格

    @ColName("c_createtime")
    private LocalDateTime createTime; // 课程创建时间

    @ColName("c_collect")
    private Integer coltSum; // 收藏数

    @ColName("c_views")
    private Integer viewSum; // 浏览数

    @ColName("c_sales")
    private Integer coSum; // 购买数

    @ColName("c_comments")
    private Integer courseComments; // 评论数

    @ColName("category_id")
    private Integer courseCategoryId; // 分类id

    // 无参构造
    public Course() {
    }

    // 有参构造
    public Course(Integer courseId, String courseName, String courseType, String courseIntro, String courseImg,
                  Double coursePrice, LocalDateTime createTime, Integer coltSum, Integer viewSum, Integer coSum,
                  Integer courseComments, Integer courseCategoryId) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.courseType = courseType;
        this.courseIntro = courseIntro;
        this.courseImg = courseImg;
        this.coursePrice = coursePrice;
        this.createTime = createTime;
        this.coltSum = coltSum;
        this.viewSum = viewSum;
        this.coSum = coSum;
        this.courseComments = courseComments;
        this.courseCategoryId = courseCategoryId;
    }

    // get和set
    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public String getCourseIntro() {
        return courseIntro;
    }

    public void setCourseIntro(String courseIntro) {
        this.courseIntro = courseIntro;
    }

    public String getCourseImg() {
        return courseImg;
    }

    public void setCourseImg(String courseImg) {
        this.courseImg = courseImg;
    }

    public Double getCoursePrice() {
        return coursePrice;
    }

    public void setCoursePrice(Double coursePrice) {
        this.coursePrice = coursePrice;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Integer getColtSum() {
        return coltSum;
    }

    public void setColtSum(Integer coltSum) {
        this.coltSum = coltSum;
    }

    public Integer getViewSum() {
        return viewSum;
    }

    public void setViewSum(Integer viewSum) {
        this.viewSum = viewSum;
    }

    public Integer getCoSum() {
        return coSum;
    }

    public void setCoSum(Integer coSum) {
        this.coSum = coSum;
    }

    public Integer getCourseComments() {
        return courseComments;
    }

    public void setCourseComments(Integer courseComments) {
        this.courseComments = courseComments;
    }

    public Integer getCourseCategoryId() {
        return courseCategoryId;
    }

    public void setCourseCategoryId(Integer courseCategoryId) {
        this.courseCategoryId = courseCategoryId;
    }


    // 测试
    @Override
    public String toString() {
        return "Course{" +
                "courseId=" + courseId +
                ", courseName='" + courseName + '\'' +
                ", courseType='" + courseType + '\'' +
                ", courseIntro='" + courseIntro + '\'' +
                ", courseImg='" + courseImg + '\'' +
                ", coursePrice=" + coursePrice +
                ", createTime=" + createTime +
                ", coltSum=" + coltSum +
                ", viewSum=" + viewSum +
                ", coSum=" + coSum +
                ", courseComments=" + courseComments +
                ", courseCategoryId=" + courseCategoryId +
                '}';
    }
}
