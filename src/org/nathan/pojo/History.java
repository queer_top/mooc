package org.nathan.pojo;

import org.nathan.Anno.ColName;

import java.time.LocalDateTime;

/**
 * 文件名称：@title: History
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:浏览历史
 * 创建日期：@date: 2024/3/28 15:13
 */
public class History {

    @ColName("h_id")
    private Integer histId; // 浏览历史id

    @ColName("h_updatetime")
    private LocalDateTime browsId; // 历史浏览时间

    @ColName("chapter_id")
    private Integer chapId; // 章节id

    @ColName("user_id")
    private Integer userId; // 用户id

    // 无参构造
    public History() {
    }

    // 有参构造
    public History(Integer histId, LocalDateTime browsId, Integer chapId, Integer userId) {
        this.histId = histId;
        this.browsId = browsId;
        this.chapId = chapId;
        this.userId = userId;
    }

    // get和set
    public Integer getHistId() {
        return histId;
    }

    public void setHistId(Integer histId) {
        this.histId = histId;
    }

    public LocalDateTime getBrowsId() {
        return browsId;
    }

    public void setBrowsId(LocalDateTime browsId) {
        this.browsId = browsId;
    }

    public Integer getChapId() {
        return chapId;
    }

    public void setChapId(Integer chapId) {
        this.chapId = chapId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    // 测试
    @Override
    public String toString() {
        return "History{" +
                "histId=" + histId +
                ", browsId=" + browsId +
                ", chapId=" + chapId +
                ", userId=" + userId +
                '}';
    }
}
