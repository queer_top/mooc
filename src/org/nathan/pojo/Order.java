package org.nathan.pojo;

import org.nathan.Anno.ColName;

import java.time.LocalDateTime;

/**
 * 文件名称：@title: Order
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:订单
 * 创建日期：@date: 2024/3/28 15:14
 */
public class Order {

    @ColName("o_id")
    private Integer coId; // 订单id

    @ColName("o_reference")
    private Integer coRefer; // 订单编号

    @ColName("o_paystate")
    private Integer coState; // 支付状态0-未支付 1-已支付

    @ColName("o_createtime")
    private LocalDateTime createTime; // 订单创建时间

    @ColName("course_id")
    private Integer courseId; // 课程id

    @ColName("user_id")
    private Integer userId; // 用户id

    // 无参构造
    public Order() {
    }

    // 有参构造
    public Order(Integer coId, Integer coRefer, Integer coState, LocalDateTime createTime, Integer courseId,
                 Integer userId) {
        this.coId = coId;
        this.coRefer = coRefer;
        this.coState = coState;
        this.createTime = createTime;
        this.courseId = courseId;
        this.userId = userId;
    }

    // get和set
    public Integer getCoId() {
        return coId;
    }

    public void setCoId(Integer coId) {
        this.coId = coId;
    }

    public Integer getCoRefer() {
        return coRefer;
    }

    public void setCoRefer(Integer coRefer) {
        this.coRefer = coRefer;
    }

    public Integer getCoState() {
        return coState;
    }

    public void setCoState(Integer coState) {
        this.coState = coState;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    // 测试
    @Override
    public String toString() {
        return "Order{" +
                "coId=" + coId +
                ", coRefer=" + coRefer +
                ", coState=" + coState +
                ", createTime=" + createTime +
                ", courseId=" + courseId +
                ", userId=" + userId +
                '}';
    }
}
