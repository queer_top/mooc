package org.nathan.pojo;

import org.nathan.Anno.ColName;

/**
 * 文件名称：@title: User
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:用户/管理员账号信息
 * 创建日期：@date: 2024/3/28 9:36
 */
public class User {

    @ColName("u_id")
    private Integer userId; // 用户id

    @ColName("u_account")
    private String account; // 账号

    @ColName("u_password")
    private String password; // 密码

    @ColName("u_nickname")
    private String nickname; // 昵称

    @ColName("u_phone")
    private String phone; // 电话

    @ColName("u_email")
    private String email; // 邮箱

    @ColName("u_sex")
    private String sex; // 性别

    @ColName("u_balance")
    private Double balance; // 余额

    @ColName("u_state")
    private Integer state; // 用户状态0-正常,1-禁用

    @ColName("u_role")
    private String role; // 用户类型，user-用户,admin-管理员

    // 无参构造函数
    public User() {
    }

    // 有参构造函数
    public User(Integer userId, String account, String password, String nickname, String phone, String email,
                String sex, Double balance, Integer state, String role) {
        this.userId = userId;
        this.account = account;
        this.password = password;
        this.nickname = nickname;
        this.phone = phone;
        this.email = email;
        this.sex = sex;
        this.balance = balance;
        this.state = state;
        this.role = role;
    }

    // get和set方法
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    // 测试
    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", nickname='" + nickname + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", sex='" + sex + '\'' +
                ", balance=" + balance +
                ", state=" + state +
                ", role='" + role + '\'' +
                '}';
    }
}
