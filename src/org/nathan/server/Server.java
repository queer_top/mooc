package org.nathan.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 文件名称：@title: Server
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:服务器
 * 创建日期：@date: 2024/3/28 9:28
 */
public class Server {
    public static void main(String[] args) {
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(50100);
            while (true) {
                System.out.println("等待连接...");
                Socket socket = serverSocket.accept();// 阻塞状态
                System.out.println("新的客户端" + socket);
                new ServerThread(socket).start();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
