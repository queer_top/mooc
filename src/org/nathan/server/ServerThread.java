package org.nathan.server;

import org.nathan.http.HttpRequest;
import org.nathan.http.HttpResponse;
import org.nathan.servlet.Servlet;
import org.nathan.servlet.ServletFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * 文件名称：@title: ServerThread
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:服务器线程
 * 创建日期：@date: 2024/3/28 9:29
 */
public class ServerThread extends Thread {
    public static Map<String, String> vcodeMap = new HashMap<>();
    private Socket socket;
    private InputStream is;
    private OutputStream os;

    public ServerThread(Socket socket) {
        this.socket = socket;
        try {
            is = socket.getInputStream();
            os = socket.getOutputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                byte buf[] = new byte[8192];
                int result = is.read(buf);
                if (result < 0) {
                    socket.close();
                    break;
                }
                String s = new String(buf, 0, result);
                //完成http协议解析
                HttpRequest httpRequest = new HttpRequest(s);
                HttpResponse httpResponse = new HttpResponse(os);
                Servlet servlet = ServletFactory.getServlet(httpRequest.getRequestUrl());
                servlet.service(httpRequest, httpResponse);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
