package org.nathan.service;

import java.util.HashMap;
import java.util.Map;

/**
 * 文件名称：@title: ServiceFactory
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:Service工厂
 * 创建日期：@date: 2024/3/29 10:02
 */
public class ServiceFactory {

    private static Map<Class, Object> serviceMap = new HashMap<>();

    static {
        serviceMap.put(UserService.class, new UserServiceImpl());
    }

    public static <T> T getService(Class cl) {
        return (T) serviceMap.get(cl);
    }
}

