package org.nathan.service;

import org.nathan.dto.Result;

/**
 * 文件名称：@title: UserService
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:抽象出UserService中相同方法的接口
 * 创建日期：@date: 2024/3/29 9:08
 */
public interface UserService {

    // 登入
    Result login(String acc, String pwd, String verCode, String vid);

    // 注册
    Result register(String acc, String pwd, String nickname, String phone, String email, String sex);

    // 返回个人信息
    Result person(Integer userId);

    // 修改密码
    Result updatePassword(Integer userId, String newPassword, String oldPassword);

}
