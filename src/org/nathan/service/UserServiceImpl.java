package org.nathan.service;

import org.nathan.dao.DaoFactory;
import org.nathan.dao.UserDao;
import org.nathan.dto.Result;
import org.nathan.pojo.User;
import org.nathan.server.ServerThread;

/**
 * 文件名称：@title: UserServiceImpl
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:实现userService接口
 * 创建日期：@date: 2024/3/29 10:03
 */
public class UserServiceImpl implements UserService {
    @Override
    public Result login(String acc, String pwd, String verCode, String vid) {
        Result result = new Result();
        // 验证码校验
        String saveCode = ServerThread.vcodeMap.get(vid); // 通过id获取保存的验证码
        System.out.println("获取的验证码" + verCode + "====" + "保存的验证码" + saveCode);
        if (!verCode.equalsIgnoreCase(saveCode)) {
            result.setStatusCode(201);// 约定验证码输入错误为201
            result.setErrMsg("验证码有误");
            return result;
        }
        // 数据库操作
        UserDao userDao = DaoFactory.getDao(UserDao.class);
        User user = userDao.queryUserByAccountAndPassword(acc, pwd);
        // 响应数据
        if (user != null) {
            result.getData().put("user", user);
        } else {
            result.setErrMsg("用户名密码有误！");
            result.setStatusCode(301);// 约定301表示登入时的错误
        }
        return result;
    }

    @Override
    public Result register(String acc, String pwd, String nickname, String phone, String email, String sex) {
        Result result = new Result();
        // 数据库操作
        UserDao userDao = DaoFactory.getDao(UserDao.class);
        if (userDao.queryUserByAccount(acc)) {
            User user = new User();
            user.setAccount(acc);
            user.setPassword(pwd);
            user.setNickname(nickname);
            user.setPhone(phone);
            user.setEmail(email);
            user.setSex(sex);
            int num = userDao.insertUser(user);
            if (num == 0) {
                result.setStatusCode(501);// 约定注册失败错误为501
                result.setErrMsg("注册失败");
            } else {
                result.getData().put("user", user);
                result.setStatusCode(200);
            }
        } else {
            result.setErrMsg("账号已存在");
            result.setStatusCode(401);// 约定账号已存在为401
        }
        return result;
    }

    @Override
    public Result person(Integer userId) {
        Result result = new Result();
        // 数据库操作
        UserDao userDao = DaoFactory.getDao(UserDao.class);
        User user = userDao.queryUserInfoByUserId(userId);
        // 响应数据
        if (user != null) {
            result.getData().put("user", user);
        } else {
            result.setErrMsg("用户id错误！");
            result.setStatusCode(701);// 约定701表示展示个人信息时错误
        }
        return result;
    }

    @Override
    public Result updatePassword(Integer userId, String newPassword, String oldPassword) {
        Result result = new Result();
        UserDao userDao = DaoFactory.getDao(UserDao.class);
        if (userDao.queryPasswordByOldPassword(userId, oldPassword)) {
            User user = new User();
            user.setPassword(newPassword);
            int num = userDao.updateNewPassword(userId, newPassword);
            if (num == 0) {
                result.setErrMsg("密码修改失败");
                result.setStatusCode(901);// 约定密码修改失败为901
            } else {
                result.getData().put("user", user);
                result.setStatusCode(200);
            }
        } else {
            result.setErrMsg("旧密码与原来不同");
            result.setStatusCode(801);// 约定旧密码不同为801
        }
        return result;
    }
}
