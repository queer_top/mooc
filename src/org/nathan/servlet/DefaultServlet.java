package org.nathan.servlet;

import org.nathan.Anno.WebServerLet;
import org.nathan.http.HttpRequest;
import org.nathan.http.HttpResponse;
import org.nathan.utils.MimeTypeUtil;

import java.io.File;
import java.io.FileInputStream;

/**
 * 文件名称：@title: DefaultSerLet
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:静态资源处理
 * 创建日期：@date: 2024/3/29 9:07
 */
@WebServerLet("/default")
public class DefaultServlet implements Servlet {

    @Override
    public void service(HttpRequest httpRequest, HttpResponse httpResponse) {
        String mimetype;
        try {
            if ("/".equals(httpRequest.getRequestUrl())) {
                httpRequest.setRequestUrl("/homepage.html");
            }
            // 静态资源处理
            File file = new File("web" + httpRequest.getRequestUrl());
            if (file.exists()) {
                mimetype = MimeTypeUtil.getMimeType(httpRequest.getRequestUrl());
                FileInputStream fi = new FileInputStream("web" + httpRequest.getRequestUrl());
                byte data[] = new byte[fi.available()];
                fi.read(data);
                httpResponse.setContentType(mimetype);
                httpResponse.write(data);
            } else {
                FileInputStream fi = new FileInputStream("web/404.html");
                byte[] data = new byte[fi.available()];
                fi.read(data);
                httpResponse.setStatusCode(404);
                httpResponse.setStatusMsg("Not Found");
                httpResponse.setContentType("text/html;charset=utf-8");
                httpResponse.write(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}