package org.nathan.servlet;

import org.nathan.http.HttpRequest;
import org.nathan.http.HttpResponse;

/**
 * 文件名称：@title: ServerLet
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:抽象出ServerLet中相同方法的接口
 * 创建日期：@date: 2024/3/29 9:07
 */
public interface Servlet {
    void service(HttpRequest httpRequest, HttpResponse httpResponse);
}
