package org.nathan.servlet;

import org.nathan.Anno.WebServerLet;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 文件名称：@title: ServerLetFactory
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:工厂设计模式优化(注解法)
 * 创建日期：@date: 2024/3/28 1:57
 */
public class ServletFactory {
    private static Map<String, Servlet> servletMap = new HashMap<>();

    static {
        // 完成ServerLet的注册
        // 扫描SeverLet包下所有带@WebServerLet注解的类，获取类上的注解的value值
        Reflections reflections = new Reflections("org.nathan.servlet");
        Set<Class<?>> typesAnnotatedWith = reflections.getTypesAnnotatedWith(WebServerLet.class);
        for (Class<?> cl : typesAnnotatedWith) {
            // 获取类上指定注解
            WebServerLet webServerLet = cl.getDeclaredAnnotation(WebServerLet.class);
            String url = webServerLet.value();
            System.out.println("url" + url);
            // 并实例化对象，将url和对应的对象保存至map中
            try {
                Object o = cl.newInstance();
                servletMap.put(url, (Servlet) o);
            } catch (InstantiationException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static Servlet getServlet(String url) {
        Servlet servlet = servletMap.get(url);
        if (servlet == null) {
            return servletMap.get("/default");
        }
        return servlet;
    }
}
