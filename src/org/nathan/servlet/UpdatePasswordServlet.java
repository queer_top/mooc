package org.nathan.servlet;

import com.alibaba.fastjson.JSON;
import org.nathan.Anno.WebServerLet;
import org.nathan.dto.Result;
import org.nathan.http.HttpRequest;
import org.nathan.http.HttpResponse;
import org.nathan.service.ServiceFactory;
import org.nathan.service.UserService;

/**
 * 文件名称：@title: UpdatePasswordServlet
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:修改密码
 * 创建日期：@date: 2024/4/1 17:35
 */

@WebServerLet("/updatePassword")
public class UpdatePasswordServlet implements Servlet {
    @Override
    public void service(HttpRequest httpRequest, HttpResponse httpResponse) {
        //获取信息
        String userId = httpRequest.getRequestParams().get("userId");
        String oldPassword = httpRequest.getRequestParams().get("oldPassword");
        String newPassword = httpRequest.getRequestParams().get("newPassword");

        // 处理
        UserService userService = ServiceFactory.getService(UserService.class);
        Result result = userService.updatePassword(Integer.valueOf(userId), newPassword, oldPassword);

        // 响应
        String s = JSON.toJSONString(result);
        httpResponse.write(s);
    }
}
