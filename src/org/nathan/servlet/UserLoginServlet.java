package org.nathan.servlet;

import com.alibaba.fastjson.JSON;
import org.nathan.Anno.WebServerLet;
import org.nathan.dto.Result;
import org.nathan.http.HttpRequest;
import org.nathan.http.HttpResponse;
import org.nathan.service.ServiceFactory;
import org.nathan.service.UserService;

/**
 * 文件名称：@title: UserLogin
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:登入业务处理
 * 创建日期：@date: 2024/3/29 11:01
 */

@WebServerLet("/userLogin")
public class UserLoginServlet implements Servlet {
    @Override
    public void service(HttpRequest httpRequest, HttpResponse httpResponse) {
        // 获取玩家账号、密码、验证码、验证码id
        String account = httpRequest.getRequestParams().get("account");
        String password = httpRequest.getRequestParams().get("password");
        String verCode = httpRequest.getRequestParams().get("verCode");
        String vid = httpRequest.getRequestParams().get("id");

        // 处理
        UserService userService = ServiceFactory.getService(UserService.class);
        Result result = userService.login(account, password, verCode, vid);

        // 响应
        String s = JSON.toJSONString(result);
        httpResponse.write(s);
    }
}
