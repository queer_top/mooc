package org.nathan.servlet;

import com.alibaba.fastjson.JSON;
import org.nathan.Anno.WebServerLet;
import org.nathan.dto.Result;
import org.nathan.http.HttpRequest;
import org.nathan.http.HttpResponse;
import org.nathan.service.ServiceFactory;
import org.nathan.service.UserService;

/**
 * 文件名称：@title: UserPerson
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:个人中心
 * 创建日期：@date: 2024/4/1 16:22
 */

@WebServerLet("/userPerson")
public class UserPersonServlet implements Servlet {

    @Override
    public void service(HttpRequest httpRequest, HttpResponse httpResponse) {
        String userId = httpRequest.getRequestParams().get("userId");

        // 处理
        UserService userService = ServiceFactory.getService(UserService.class);
        Result result = userService.person(Integer.valueOf(userId));

        // 响应
        String s = JSON.toJSONString(result);
        httpResponse.write(s);
    }
}
