package org.nathan.servlet;

import com.alibaba.fastjson.JSON;
import org.nathan.Anno.WebServerLet;
import org.nathan.dto.Result;
import org.nathan.http.HttpRequest;
import org.nathan.http.HttpResponse;
import org.nathan.service.ServiceFactory;
import org.nathan.service.UserService;

/**
 * 文件名称：@title: UserRegister
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:注册业务处理
 * 创建日期：@date: 2024/3/29 11:18
 */

@WebServerLet("/userRegister")
public class UserRegisterServlet implements Servlet {
    @Override
    public void service(HttpRequest httpRequest, HttpResponse httpResponse) {
        // 获取玩家输入内容
        String acc = httpRequest.getRequestParams().get("useracc");
        String pwd = httpRequest.getRequestParams().get("password");
        String nickName = httpRequest.getRequestParams().get("username");
        String phone = httpRequest.getRequestParams().get("phonenum");
        String email = httpRequest.getRequestParams().get("email");
        String sex = httpRequest.getRequestParams().get("usersex");

        // 处理
        UserService userService = ServiceFactory.getService(UserService.class);
        Result result = userService.register(acc, pwd, nickName, phone, email, sex);

        // 响应
        String s = JSON.toJSONString(result);
        httpResponse.write(s);
    }
}
