package org.nathan.servlet;

import org.nathan.Anno.WebServerLet;
import org.nathan.http.HttpRequest;
import org.nathan.http.HttpResponse;
import org.nathan.server.ServerThread;
import org.nathan.utils.CaptchaUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * 文件名称：@title: VerCodeServerLet
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:获取验证码
 * 创建日期：@date: 2024/3/29 9:07
 */
@WebServerLet("/VerCode")
public class VerCodeServlet implements Servlet {

    @Override
    public void service(HttpRequest httpRequest, HttpResponse httpResponse) {
        CaptchaUtil captcha = new CaptchaUtil();
        String codeId = httpRequest.getRequestParams().get("id");
        String code = captcha.getCode();
        System.out.println("验证码id：" + codeId);
        System.out.println("验证码：" + code);
        // 存入验证码
        ServerThread.vcodeMap.put(codeId, code);
        BufferedImage image = captcha.getImage();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            // 将图片写入ByteArrayOutputStream字节数组输出流
            ImageIO.write(image, "png", bos);
            // 从字节数组输出流中获取字节数组
            byte[] data = bos.toByteArray();
            httpResponse.setContentType("image/png");
            httpResponse.write(data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

