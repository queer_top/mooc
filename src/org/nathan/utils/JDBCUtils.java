package org.nathan.utils;

import com.alibaba.druid.pool.DruidDataSource;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 * 文件名称：@title: utils
 * 项目名称：@projectName: mooc
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:连接池
 * 创建日期：@date: 2024/3/27 09:15
 */
public class JDBCUtils {

    private static final String DB_CONFIG_FILE = "db.properties";
    private static DruidDataSource ds = new DruidDataSource();

    static {
        Properties properties = new Properties();
        try (FileInputStream fis = new FileInputStream(DB_CONFIG_FILE)) {
            properties.load(fis);
            ds.setUrl(properties.getProperty("url"));
            ds.setUsername(properties.getProperty("username"));
            ds.setPassword(properties.getProperty("password"));
            ds.setInitialSize(new Integer(properties.getProperty("initialSize")));
            ds.setMaxActive(new Integer(properties.getProperty("maxActive")));
            ds.setMinIdle(new Integer(properties.getProperty("minIdle")));
            ds.setMaxWait(new Long(properties.getProperty("maxWait")));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("创建连接池失败：" + e.getMessage());
        }

    }

    // 创建连接
    public static Connection createConn() {
        try {
            return ds.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("获取连接失败:" + e.getMessage());
        }
    }

    // 关闭连接
    public static void closeConnect(Connection connection, PreparedStatement statement, ResultSet rs) {
        try {
            if (connection != null) {
                connection.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
