package org.nathan.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 文件名称：@title: MimeTypeUtil
 * 项目名称：@projectName: Homework0313
 * 项目作者：@author: Nathan_Queer——Top
 * 项目描述：@description:互联网媒体类型
 * 创建日期：@date: 2024/3/27 09:15
 */
public class MimeTypeUtil {
    private static Map<String, String> map = new HashMap<>();

    static {
        map.put("html", "text/html;charset=utf-8");
        map.put("css", "text/css;charset=utf-8");
        map.put("js", "application/javascript;charset=utf-8");
        map.put("png", "image/png");
    }

    public static String getMimeType(String fileName) {
        //获取文件的后缀名
        int index = fileName.lastIndexOf(".");
        String suffixName = fileName.substring(index + 1);
        //根据后缀名返回它的MimeType类型
        return map.get(suffixName);
    }
}
