// 状态改变
var user = JSON.parse(localStorage.getItem('user'));
if (user !== null) {
    let userElement = document.getElementById('user');
    let s = `
    <img src="../img/icon.jpg" alt=""style="width: 40px;height: 40px;margin-left: 10px; padding:0;border-radius: 50%;">
    <a href="../userCenter.html">${user.nickname}</a>
    <button onclick="toLogin()" style="background-color: rgba(0, 0, 0, 0.1);border-style: hidden;border-radius: 4px;
    width: auto; height: auto; cursor: pointer;">退出登入</button>`;
    userElement.innerHTML = s;
}

// 退出登入
function toLogin() {
    localStorage.removeItem('user');
    alert("账号已退出")
    location.href = "homepage.html";
}

// 假设你有一个包含课程信息的数组 coursesData
function generateCardHTML(productData) {
    // 假设 productData 是从数据库获取的产品数据对象
    const {
        productName,
        purchaseCount,
        price
    } = productData; // 假设产品数据对象中包含产品名称、购买人数和价格等信息

    // 使用字符串模板拼接出相应的 HTML 结构
    const cardHTML = `
		        <div class="card">
		            <img src="./img/class.png" alt="Product Image">
		            <div class="card-content">
		                <h4>${productName}</h4>
		                <h6>购买人数: ${purchaseCount}</h6>
		                <p class="price">￥${price}</p>
		                <button class="shopping">立即购买</button>
		            </div>
		        </div>
		    `;
    return cardHTML; // 返回拼接好的 HTML 字符串
}

// 个人中心判断
function queryUser() {
    if (user === null) {
        alert("您还未登入，请登入！")
    } else {
        location.href = "userCenter.html";
    }
}

