// 判断登入，首界面打开,登入页面关闭
function openHomePage() {
    let vid = 0;
    let account = $('#account').val().trim();
    let password = $('#password').val().trim();
    let verCode = $("#inputCode").val().trim();
    // 前端非空判断
    if (account === "" || password === "") {
        alert('请输入账号密码！');
        return;
    }
    if (verCode === "") {
        alert('请输入验证码！');
        return;
    }

    //浏览器访问服务器，服务器中进行数据库的验证
    $.ajax("/userLogin", {
        data: {
            account: account,
            password: password,
            verCode: verCode,
            vid: vid
        },
        type: "post",
        success: function (data) {
            // 释放浏览器本地资源
            localStorage.removeItem("user");
            let user = data.data.user;
            if (data.statusCode === 200) {
                alert("登入成功")
                // 将登入用户存入浏览器本地内存
                let userString = JSON.stringify(user);
                localStorage.setItem("user", userString);
                location.href = "homepage.html";
                window.close();
            } else {
                alert(data.errMsg)
                flushCode();
            }
        }
    })
}

flushCode();

// 注册界面打开,登入界面关闭
function openRegister() {
    location.href = "frontRegister.html";
    window.close();
}

function flushCode() {
    vid = new Date().getTime();
    $("#Code").prop("src", "/VerCode?id=" + vid)
}
