// 手机号码正则：国标标准
var telregular = /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/;
// 邮箱正则：邮箱标准
var emailregular = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

// 判断注册，登入界面打开,注册界面关闭
function openLogin() {
    let useracc = $('#useracc').val().trim();
    let username = $('#username').val().trim();
    let password = $('#password').val().trim();
    let surepassword = $('#surepassword').val().trim();
    let phonenum = $('#phonenum').val().trim();
    let email = $('#email').val().trim();
    let sex = $(":radio:checked").val();

    if (useracc === "" || username === "" || password === "" || surepassword === "" || phonenum === "" || email === "" || sex === null) {
        alert('请填写完整信息后再注册！');
        return;
    }
    if (password !== surepassword) {
        alert('两次密码不一致！');
        return;
    }

    if (!telregular.test(phonenum)) {
        alert("请输入正确的手机号！");
        return;
    }

    if (!emailregular.test(email)) {
        alert("请输入正确的邮箱！");
        return;
    }

    $.ajax("/userRegister", {
        data: {
            useracc: useracc,
            username: username,
            password: password,
            usersex: sex,
            phonenum: phonenum,
            email: email
        },
        type: "post",
        success: function (data) {
            if (data.statusCode === 200) {
                alert("注册成功")
                location.href = "frontLogin.html"
            } else {
                alert(data.errMsg)
            }
        }
    })
}