var doms = {
    carousel: document.querySelector('.carousel'),
    indicators: document.querySelectorAll('.indicator span'),
    prevBtn: document.querySelector('.prev'),
    nextBtn: document.querySelector('.next')
};
var index = 0;
var timer;

var user = JSON.parse(localStorage.getItem('user'));

//移动轮播图到第几个位置
function moveTo(index) {
    doms.carousel.style.transform = `translateX(-${index}00%)`;
    //去除当前选中的指示器
    var active = document.querySelector('.indicator span.active');
    active.classList.remove('active');
    //重新设置要选中的指示器
    doms.indicators[index].classList.add('active');
}

doms.indicators.forEach(function (item, i) {
    item.onclick = function () {
        index = i;
        moveTo(i);
    }
})
doms.prevBtn.onclick = function () {
    index = (index - 1 + 6) % 6;
    moveTo(index);
}

doms.nextBtn.onclick = function () {
    index = (index + 1) % 6;
    moveTo(index);
}

//自动播放
function autoMove() {
    timer = setInterval(function () {
        index++;
        if (index > 5) {
            index = 0;
        }
        moveTo(index);
    }, 2000)
}

autoMove()
//鼠标悬停在轮播图上，停止自动播放
doms.carousel.onmouseover = function () {
    clearInterval(timer);
}
// 鼠标离开轮播图，继续自动播放
doms.carousel.onmouseleave = function () {
    autoMove(); // 重新启动自动播放
}

// 课程详情页
function toDetails() {
    location.href = "details.html";
}

// 状态改变
if (user !== null) {
    let userElement = document.getElementById('user');
    let s = `
    <img src="../img/icon.jpg" alt=""style="width: 40px;height: 40px;margin-left: 10px; padding:0;border-radius: 50%;">
    <a href="../userCenter.html">${user.nickname}</a>
    <button onclick="toLogin()" style="background-color: rgba(0, 0, 0, 0.1);border-style: hidden;border-radius: 4px;
    width: auto; height: auto; cursor: pointer;">退出登入</button>`;
    userElement.innerHTML = s;
}

// 退出登入
function toLogin() {
    localStorage.removeItem('user');
    alert("账号已退出")
    location.href = "homepage.html";
}

// 个人中心判断
function queryUser() {
    if (user === null) {
        alert("您还未登入，请先登入！")
    } else {
        location.href = "userCenter.html";
    }
}