// 状态改变
var user = JSON.parse(localStorage.getItem('user'));
if (user !== null) {
    let userElement = document.getElementById('user');
    let s = `
    <img src="../img/icon.jpg" alt=""style="width: 40px;height: 40px;margin-left: 10px; padding:0;border-radius: 50%;">
    <a href="../userCenter.html">${user.nickname}</a>
    <button onclick="toLogin()" style="background-color: rgba(0, 0, 0, 0.1);border-style: hidden;border-radius: 4px;
    width: auto; height: auto; cursor: pointer;">退出登入</button>`;
    userElement.innerHTML = s;
}

// 退出登入
function toLogin() {
    localStorage.removeItem('user');
    location.href = "frontLogin.html";
}

document.addEventListener("DOMContentLoaded", function () {
    var lis = document.querySelectorAll("li");
    var overlay = document.querySelector(".overlay");
    var cancelButton = document.getElementById('cancelButton');

    function openOverlay() {
        overlay.style.display = "block";
    }

    function closeOverlay() {
        overlay.style.display = "none";
    }

    lis.forEach(function (li) {
        li.addEventListener("click", openOverlay);
    });

    overlay.addEventListener("click", function (event) {
        if (event.target === overlay) {
            closeOverlay();
        }
    });

    function ok() {
        const confirmButton = document.getElementById('confirmButton');

        confirmButton.addEventListener('click', function () {
            const amount = document.getElementById('rechargeAmount').value;

            if (!amount) {
                alert('请输入充值金额！');
                return;
            }

            const isConfirmed = confirm(`确定充值 ${amount} 吗?`);

            if (isConfirmed) {
                alert('充值成功！');
                closeOverlay();
            } else {
                alert('取消充值。');
                closeOverlay();
            }
        });
    }

    ok();

    // 点击取消充值按钮触发自定义事件
    cancelButton.addEventListener('click', function () {
        var cancelEvent = new CustomEvent('cancelRecharge');
        document.dispatchEvent(cancelEvent);
    });

    // 监听取消充值事件并关闭遮罩层
    document.addEventListener('cancelRecharge', function () {
        closeOverlay();
    });
});