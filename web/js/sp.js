// 状态改变
var user = JSON.parse(localStorage.getItem('user'));
if (user !== null) {
    let userElement = document.getElementById('user');
    let s = `
    <img src="../img/icon.jpg" alt=""style="width: 40px;height: 40px;margin-left: 10px; padding:0;border-radius: 50%;">
    <a href="../userCenter.html">${user.nickname}</a>
    <button onclick="toLogin()" style="background-color: rgba(0, 0, 0, 0.1);border-style: hidden;border-radius: 4px;
    width: auto; height: auto; cursor: pointer;">退出登入</button>`;
    userElement.innerHTML = s;
}

// 退出登入
function toLogin() {
    localStorage.removeItem('user');
    alert("账号已退出")
    location.href = "homepage.html";
}