// 状态改变
var user = JSON.parse(localStorage.getItem('user'));
if (user !== null) {
    let userElement = document.getElementById('user');
    let s = `
    <img src="../img/icon.jpg" alt=""style="width: 40px;height: 40px;margin-left: 10px; padding:0;border-radius: 50%;">
    <a href="../userCenter.html">${user.nickname}</a>
    <button onclick="toLogin()" style="background-color: rgba(0, 0, 0, 0.1);border-style: hidden;border-radius: 4px;
    width: auto; height: auto; cursor: pointer;">退出登入</button>`;
    userElement.innerHTML = s;
}

// 退出登入
function toLogin() {
    localStorage.removeItem('user');
    alert("账号已退出")
    location.href = "homepage.html";
}

function showContent(contentId) {
    var contents = document.getElementsByClassName('content');
    for (var i = 0; i < contents.length; i++) {
        contents[i].style.display = 'none';
    }
    document.getElementById(contentId).style.display = 'block';
}


// 修改密码
function showPop() {
    var modal = document.getElementById("pop");
    modal.style.display = "block";
}

function closePop() {
    var modal = document.getElementById("pop");
    modal.style.display = "none";
}


// 个人信息展示
function showPerson() {
    var modal = document.getElementById('personalInfo');
    console.log(modal)
    modal.style.display = 'block';
    $.ajax("/userPerson", {
            data: {
                userId: user.userId
            },
            type: "post",
            success: function (data) {
                console.log("person", data);
                let name = $('#name').val();
                var person = data.data.user
                var s = `<div class="content" id="person">
                    <h2>个人信息</h2>
                    <div>
                        <p>
                            <label>昵称：</label>
                            <span id="name">${person.nickname}</span>
                        </p>
                        <p>
                            <label>账号：</label>
                            <span id="acc">${person.account}</span>
                        </p>
                        <p>
                            <label>性别：</label>
                            <span id="sex">${person.sex}</span>
                        </p>
                        <p>
                            <label>密码：</label>
                            <button onclick="showPop()">修改密码</button>
                        </p>
                        <p>
                            <label>电话：</label>
                            <span id="phone">${person.phone}</span>
                        </p>
                        <p>
                            <label>邮箱：</label>
                            <span id="mail">${person.email}</span>
                        </p>
                        <p>
                            <label>余额：</label>
                            <span id="amount">${person.balance}元</span>
                        </p>
                    </div>
                </div>`
                let personElement = document.getElementById("personalInfo");
                personElement.innerHTML = s;
            }
        }
    )
}

showPerson();

// 修改密码的方法
function updatePwd() {
    var model = document.getElementById("pop");

    var oldPwd = $('#oldPassword').val().trim();
    var newPwd = $('#newPassword').val().trim();
    var confirmPwd = $('#confirmPassword').val().trim();
    console.log(oldPwd);
    if (oldPwd === "" || newPwd === "" || confirmPwd === "") {
        alert("请填写完整");
        return
    }
    if (oldPwd === newPwd) {
        alert("新旧密码一致")
        return
    }
    if (newPwd !== confirmPwd) {
        alert("两次输入的密码不一致")
        return
    }
    $.ajax("/updatePassword", {
        data: {
            userId: user.userId,
            oldPassword: oldPwd,
            newPassword: newPwd
        },
        type: "post",
        success: function (data) {
            console.log("updatePassword", data);
            if (data.statusCode === 200) {
                alert("修改成功,请重新登录！");
                closePop();
            } else if (data.statusCode === 801) {
                alert(data.errMsg);
                return
            } else {
                alert("修改失败");
                return
            }
            //刷新状态 重新登录
            localStorage.removeItem('user');
            window.location.href = "./frontLogin.html";
        }
    });
    model.style.display = "block";
}